#!/bin/bash

FILE="$WORKDIR/recursive/values.json"

jq_expect "$FILE" '$..a' '[[0,1,2],["a","b","c"]]'
jq_expect "$FILE" '$..[?($index == "a")]' '[[0,1,2],["a","b","c"]]'
jq_expect "$FILE" '$..[?(@.type == "one")]' '[{"a":["a","b","c"],"type":"one"},{"b":["1","3","6"],"type":"one"}]'
jq_expect "$FILE" '$..[?(@ is string)]' '["one","a","b","c","one","1","3","6"]'