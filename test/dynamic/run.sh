#!/bin/bash

FILE="$WORKDIR/dynamic/values.json"

jq_expect "$FILE" '$.a[$.b]' '"a"'

# Get last item of array
jq_expect "$FILE" '$.a[$length - 1]' '"e"'

# Filters
jq_expect "$FILE" '$.a[?(false)]' '[]'
jq_expect "$FILE" '$.a[?($index >= 1 && $index <= 2)]' '["b","c"]'
jq_expect "$FILE" '$[?("a" in @)]' '{"a":["a","b","c","d","e"]}'
jq_expect "$FILE" '$ is object' 'true'
jq_expect "$FILE" '$ is array' 'false'
jq_expect "$FILE" '$.a is object' 'false'
jq_expect "$FILE" '$.a is array' 'true'
