#!/bin/bash

CMD="$JQ_EXEC -f \"$WORKDIR/cli-all-in-one/all-in-one.json\" --all-in-one"

echo "--> Execute: $CMD"
result="$(eval "$CMD")"
echo "$result"
echo "<--> Expect"
echo "0"
if [ "$result" != "0" ]; then
  echo "<-- Result mismatch"
  exit 1
else
  echo "<-- Result match"
fi;
echo ""