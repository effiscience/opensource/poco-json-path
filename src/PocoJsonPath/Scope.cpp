//
// Created by desrumaux on 23/11/2021.
//

#include "Scope.hpp"

namespace PocoJsonPath {

    std::shared_ptr<Scope> Scope::NewRootScope(std::shared_ptr<Engine> engine, Poco::Dynamic::Var variable)
    {
        return std::make_shared<Scope>(engine, nullptr, nullptr, variable);
    }

    std::shared_ptr<Scope> Scope::NewScope(std::shared_ptr<Scope> parent, Poco::Dynamic::Var variable)
    {
        std::shared_ptr<Scope> child;
        if (parent->isRootScope()) {
            child = std::make_shared<Scope>(parent->getEngine(), parent, parent, variable);
        } else {
            child = std::make_shared<Scope>(parent->getEngine(), parent->getRoot(), parent, variable);
        }
        child->setVariables(parent->getVariables());
        parent->children.push_back(parent);
        return child;
    }

    Scope::Scope(std::shared_ptr<Engine> engine, std::shared_ptr<Scope> root, std::shared_ptr<Scope> parent, Poco::Dynamic::Var value)
        : engine(engine)
        , root(root)
        , parent(parent)
        , value(value)
        , children()
        , variables()
    {}

    bool Scope::isRootScope() const
    {
        return nullptr == root && nullptr == parent;
    }

    std::shared_ptr<Engine> Scope::getEngine()
    {
        return engine;
    }

    std::shared_ptr<Scope> Scope::getRoot()
    {
        if (isRootScope()) {
            throw Poco::LogicException("JsonPath: Scope is already root scope.");
        }

        return root;
    }

    std::shared_ptr<Scope> Scope::getParent()
    {
        if (isRootScope()) {
            throw Poco::LogicException("JsonPath: Scope doesn't have parent because it's root scope.");
        }

        return root;
    }

    Poco::Dynamic::Var Scope::getValue()
    {
        return value;
    }

    void Scope::setValue(Poco::Dynamic::Var value)
    {
        this->value = value;
    }

    std::map<std::string, Poco::Dynamic::Var> Scope::getVariables()
    {
        return variables;
    }

    void Scope::setVariables(std::map<std::string, Poco::Dynamic::Var> variables)
    {
        this->variables = variables;
    }

    bool Scope::hasVariable(std::string name) const
    {
        return variables.find(name) != variables.end();
    }

    Poco::Dynamic::Var Scope::getVariable(std::string name)
    {
        if (!hasVariable(name)) {
            throw std::logic_error("Undefined variable: " + name);
        }
        return variables[name];
    }

    void Scope::setVariable(std::string name, Poco::Dynamic::Var variable)
    {
        variables[name] = variable;
    }

    void Scope::deleteVariable(std::string name)
    {
        if (!hasVariable(name)) {
            return;
        }

        variables.erase(name);
    }
}
