//
// Created by desrumaux on 23/11/2021.
//

#include "Engine.hpp"

#include <stdexcept>

#include "Scope.hpp"
#include "Keyword.hpp"
#include "Operators.hpp"
#include "Filters.hpp"

#include "Peg/AbstractPrePegHandler.hpp"
#include "Peg/StringPrePegHandler.hpp"
#include "Peg/NumberPrePegHandler.hpp"
#include "Peg/IndexPrePegHandler.hpp"
#include "Peg/TemplatePrePegHandler.hpp"
#include "Peg/RootPegHandler.hpp"
#include "Peg/CurrentPegHandler.hpp"
#include "Peg/ChainedValuePrePegHandler.hpp"
#include "Peg/OperatorPrePegHandler.hpp"
#include "Peg/VariablePrePegHandler.hpp"
#include "Peg/ConstantePrePegHandler.hpp"
#include "Peg/PipePrePegHandler.hpp"
#include "Peg/FilterPrePegHandler.hpp"
#include "Peg/RecursiveChainedValuePrePegHandler.hpp"
#include "Peg/ArrayPrePegHandler.hpp"
#include "Peg/ObjectPrePegHandler.hpp"

#include "Operators/AddOperator.hpp"
#include "Operators/SubstractOperator.hpp"
#include "Operators/MultOperator.hpp"
#include "Operators/DivOperator.hpp"
#include "Operators/AndOperator.hpp"
#include "Operators/OrOperator.hpp"
#include "Operators/EqualOperator.hpp"
#include "Operators/NotEqualOperator.hpp"
#include "Operators/GreaterThanOperator.hpp"
#include "Operators/GreaterThanOrEqualOperator.hpp"
#include "Operators/LowerThanOperator.hpp"
#include "Operators/LowerThanOrEqualOperator.hpp"
#include "Operators/IsOperator.hpp"
#include "Operators/InOperator.hpp"

#include "Filters/IFilter.hpp"
#include "Filters/StringifyFilter.hpp"
#include "Filters/ParseFilter.hpp"
#include "Filters/QuoteFilter.hpp"
#include "Filters/UnquoteFilter.hpp"

namespace PocoJsonPath {

    std::shared_ptr<Engine> Engine::New()
    {
        auto ptr =  std::make_shared<Engine>();

        // Peg handler
        auto stringPrePegHandler = std::make_shared<Peg::StringPrePegHandler>(ptr);
        auto numberPrePegHandler = std::make_shared<Peg::NumberPrePegHandler>(ptr);
        auto indexPrePegHandler = std::make_shared<Peg::IndexPrePegHandler>(ptr);
        auto rootPrePegHandler = std::make_shared<Peg::TemplatePrePegHandler<Peg::RootPegHandler>>(ptr);
        auto currentPrePegHandler = std::make_shared<Peg::TemplatePrePegHandler<Peg::CurrentPegHandler>>(ptr);
        auto chainedValuePrePegHandler = std::make_shared<Peg::ChainedValuePrePegHandler>(ptr);
        auto operatorPrePegHandler = std::make_shared<Peg::OperatorPrePegHandler>(ptr);
        auto userVariablePrePegHandler = std::make_shared<Peg::VariablePrePegHandler>(ptr, true);
        auto scopeVariablePrePegHandler = std::make_shared<Peg::VariablePrePegHandler>(ptr, false);
        auto pipePrePegHandler = std::make_shared<Peg::PipePrePegHandler>(ptr);
        auto filterPrePegHandler = std::make_shared<Peg::FilterPrePegHandler>(ptr);
        auto recursiveIndexValuePrePegHandler = std::make_shared<Peg::RecursiveChainedValuePrePegHandler>(ptr, false);
        auto recursiveFilterValuePrePegHandler = std::make_shared<Peg::RecursiveChainedValuePrePegHandler>(ptr, true);

        ptr->setHandler(Keyword::PJQ_ROOT, rootPrePegHandler);
        ptr->setHandler(Keyword::PJQ_CURRENT, currentPrePegHandler);
        ptr->setHandler(Keyword::PJQ_QUERY, chainedValuePrePegHandler);

        ptr->setHandler(Keyword::PJQ_STRING, stringPrePegHandler);
        ptr->setHandler(Keyword::PJQ_NUMBER, numberPrePegHandler);
        ptr->setHandler(Keyword::PJQ_USER_VARIABLE, userVariablePrePegHandler);
        ptr->setHandler(Keyword::PJQ_SCOPED_VARIABLE, scopeVariablePrePegHandler);
        ptr->setHandler(Keyword::PJQ_TRUE, std::make_shared<Peg::ConstantePrePegHandler>(ptr, true));
        ptr->setHandler(Keyword::PJQ_FALSE, std::make_shared<Peg::ConstantePrePegHandler>(ptr, false));
        ptr->setHandler(Keyword::PJQ_KEYWORD_NULL, std::make_shared<Peg::ConstantePrePegHandler>(ptr, Poco::Dynamic::Var{}));
        ptr->setHandler(Keyword::PJQ_ARRAY, std::make_shared<Peg::ArrayPrePegHandler>(ptr));
        ptr->setHandler(Keyword::PJQ_OBJECT, std::make_shared<Peg::ObjectPrePegHandler>(ptr));

        ptr->setHandler(Keyword::PJQ_KEYWORD, stringPrePegHandler);
        ptr->setHandler(Keyword::PJQ_NUMBER_KEYWORD, numberPrePegHandler);

        ptr->setHandler(Keyword::PJQ_INDEX, indexPrePegHandler);
        ptr->setHandler(Keyword::PJQ_DYNAMIC_INDEX, indexPrePegHandler);
        ptr->setHandler(Keyword::PJQ_FILTER, filterPrePegHandler);
        ptr->setHandler(Keyword::PJQ_PIPE, pipePrePegHandler);

        ptr->setHandler(Keyword::PJQ_RECURSIVE_INDEX, recursiveIndexValuePrePegHandler);
        ptr->setHandler(Keyword::PJQ_RECURSIVE_FILTER, recursiveFilterValuePrePegHandler);

        ptr->setHandler(Keyword::PJQ_SCRIPT, chainedValuePrePegHandler);
        ptr->setHandler(Keyword::PJQ_SCRIPT_LOGIC_1, operatorPrePegHandler);
        ptr->setHandler(Keyword::PJQ_SCRIPT_LOGIC_1_OPERATOR, stringPrePegHandler);
        ptr->setHandler(Keyword::PJQ_SCRIPT_LOGIC_IS_OPERATOR, stringPrePegHandler);
        ptr->setHandler(Keyword::PJQ_SCRIPT_LOGIC_2, operatorPrePegHandler);
        ptr->setHandler(Keyword::PJQ_SCRIPT_LOGIC_2_OPERATOR, stringPrePegHandler);
        ptr->setHandler(Keyword::PJQ_SCRIPT_MATH_1, operatorPrePegHandler);
        ptr->setHandler(Keyword::PJQ_SCRIPT_MATH_1_OPERATOR, stringPrePegHandler);
        ptr->setHandler(Keyword::PJQ_SCRIPT_MATH_2, operatorPrePegHandler);
        ptr->setHandler(Keyword::PJQ_SCRIPT_MATH_2_OPERATOR, stringPrePegHandler);

        // Operator handler
        auto addOperatorHandler = std::make_shared<Operators::AddOperator>();
        auto substractOperatorHandler = std::make_shared<Operators::SubstractOperator>();
        auto multOperatorHandler = std::make_shared<Operators::MultOperator>();
        auto divideOperatorHandler = std::make_shared<Operators::DivOperator>();
        auto andOperatorHandler = std::make_shared<Operators::AndOperator>();
        auto orOperatorHandler = std::make_shared<Operators::OrOperator>();
        auto equalOperatorHandler = std::make_shared<Operators::EqualOperator>();
        auto notEqualOperatorHandler = std::make_shared<Operators::NotEqualOperator>();
        auto greaterOperatorHandler = std::make_shared<Operators::GreaterThanOperator>();
        auto greaterOrEqualOperatorHandler = std::make_shared<Operators::GreaterThanOrEqualOperator>();
        auto lowerOperatorHandler = std::make_shared<Operators::LowerThanOperator>();
        auto lowerOrEqualOperatorHandler = std::make_shared<Operators::LowerThanOrEqualOperator>();
        auto isOperatorHandler = std::make_shared<Operators::IsOperator>();
        auto inOperatorHandler = std::make_shared<Operators::InOperator>();

        ptr->setOperatorHandler(Operators::ADD, addOperatorHandler);
        ptr->setOperatorHandler(Operators::SUBSTRACT, substractOperatorHandler);
        ptr->setOperatorHandler(Operators::MULT, multOperatorHandler);
        ptr->setOperatorHandler(Operators::DIVIDE, divideOperatorHandler);
        ptr->setOperatorHandler(Operators::AND, andOperatorHandler);
        ptr->setOperatorHandler(Operators::OR, orOperatorHandler);
        ptr->setOperatorHandler(Operators::EQUAL, equalOperatorHandler);
        ptr->setOperatorHandler(Operators::NOT_EQUAL, notEqualOperatorHandler);
        ptr->setOperatorHandler(Operators::GREATER_THAN, greaterOperatorHandler);
        ptr->setOperatorHandler(Operators::GREATER_THAN_OR_EQUAL, greaterOrEqualOperatorHandler);
        ptr->setOperatorHandler(Operators::LOWER_THAN, lowerOperatorHandler);
        ptr->setOperatorHandler(Operators::LOWER_THAN_OR_EQUAL, lowerOrEqualOperatorHandler);
        ptr->setOperatorHandler(Operators::IN, inOperatorHandler);
        ptr->setOperatorHandler(Operators::IS, isOperatorHandler);

        ptr->setFilterHandler(Filters::stringify, std::make_shared<Filters::StringifyFilter>());
        ptr->setFilterHandler(Filters::parse, std::make_shared<Filters::ParseFilter>());
        ptr->setFilterHandler(Filters::quote, std::make_shared<Filters::QuoteFilter>());
        ptr->setFilterHandler(Filters::unquote, std::make_shared<Filters::UnquoteFilter>());

        return ptr;
    }

    Engine::Engine()
        : parser(R"(
            ENTRYPOINT                      <- PJQ_SCRIPT

            PJQ_QUERY                       <- (PJQ_ROOT/PJQ_CURRENT) PJQ_CHAIN*
            PJQ_ANY                         <- '(' PJQ_ANY ')' / PJQ_VALUE / PJQ_QUERY

            PJQ_CHAIN                       <- (PJQ_RECURSIVE_INDEX/PJQ_RECURSIVE_FILTER/PJQ_DYNAMIC_INDEX/PJQ_INDEX/PJQ_FILTER/PJQ_PIPE)
            PJQ_RECURSIVE_INDEX             <- ('.' PJQ_INDEX / '..' PJQ_DYNAMIC_INDEX) PJQ_CHAIN*
            PJQ_RECURSIVE_FILTER            <- '..' PJQ_FILTER PJQ_CHAIN*

            PJQ_INDEX                       <- [.] (PJQ_STRING/PJQ_NUMBER_KEYWORD/PJQ_KEYWORD)
            PJQ_DYNAMIC_INDEX               <- '[' PJQ_SCRIPT ']'
            PJQ_FILTER                      <- '[' '?' '(' PJQ_SCRIPT ')' ']'
            PJQ_PIPE                        <- '|' PJQ_KEYWORD PJQ_BRACKET(PJQ_SCRIPT (',' PJQ_SCRIPT)*) / '|' PJQ_KEYWORD PJQ_BRACKET('') / '|' PJQ_KEYWORD

            PJQ_SCRIPT                      <- (PJQ_SCRIPT_BODY / PJQ_ANY) PJQ_CHAIN*
            PJQ_SCRIPT_BODY                 <- PJQ_SCRIPT_LOGIC / PJQ_SCRIPT_MATH / PJQ_ANY
            PJQ_SCRIPT_LOGIC                <- PJQ_SCRIPT_LOGIC_2 / PJQ_SCRIPT_LOGIC_1
            PJQ_SCRIPT_LOGIC_1              <- (PJQ_BRACKET(PJQ_SCRIPT)/PJQ_SCRIPT_MATH/PJQ_ANY) (PJQ_SCRIPT_LOGIC_1_OPERATOR (PJQ_BRACKET(PJQ_SCRIPT)/PJQ_SCRIPT_MATH/PJQ_ANY))+ / (PJQ_BRACKET(PJQ_SCRIPT)/PJQ_SCRIPT_MATH/PJQ_ANY) PJQ_SCRIPT_LOGIC_IS_OPERATOR PJQ_KEYWORD
            PJQ_SCRIPT_LOGIC_1_OPERATOR     <- '==' / '!=' / '>=' / '>' / '<=' / '<' / 'in'
            PJQ_SCRIPT_LOGIC_IS_OPERATOR    <- 'is'
            PJQ_SCRIPT_LOGIC_2              <- (PJQ_BRACKET(PJQ_SCRIPT)/PJQ_SCRIPT_LOGIC_1/PJQ_ANY) (PJQ_SCRIPT_LOGIC_2_OPERATOR (PJQ_BRACKET(PJQ_SCRIPT)/PJQ_SCRIPT_LOGIC_1/PJQ_ANY))+
            PJQ_SCRIPT_LOGIC_2_OPERATOR     <- '&&' / '||'
            PJQ_SCRIPT_MATH                 <- PJQ_SCRIPT_MATH_2 / PJQ_SCRIPT_MATH_1
            PJQ_SCRIPT_MATH_1               <- (PJQ_BRACKET(PJQ_SCRIPT)/PJQ_ANY) (PJQ_SCRIPT_MATH_1_OPERATOR (PJQ_BRACKET(PJQ_SCRIPT)/PJQ_ANY))+
            PJQ_SCRIPT_MATH_1_OPERATOR      <- '*' / '/'
            PJQ_SCRIPT_MATH_2               <- (PJQ_BRACKET(PJQ_SCRIPT)/PJQ_SCRIPT_MATH_1/PJQ_ANY) (PJQ_SCRIPT_MATH_2_OPERATOR (PJQ_BRACKET(PJQ_SCRIPT)/PJQ_SCRIPT_MATH_1/PJQ_ANY))+
            PJQ_SCRIPT_MATH_2_OPERATOR      <- '+' / '-'

            PJQ_ROOT                        <- '$'
            PJQ_CURRENT                     <- '@'

            PJQ_KEYWORD                     <- < [A-Za-z_]+ >
            PJQ_NUMBER_KEYWORD              <- < [0-9]+ >

            PJQ_VALUE                       <- PJQ_STRING / PJQ_NUMBER / PJQ_BOOLEAN / PJQ_NULL / PJQ_SCOPED_VARIABLE / PJQ_USER_VARIABLE / PJQ_ARRAY / PJQ_OBJECT
            PJQ_SCOPED_VARIABLE             <- < '$' (PJQ_STRING/PJQ_KEYWORD) >
            PJQ_USER_VARIABLE               <- < '%' (PJQ_STRING/PJQ_KEYWORD) >
            PJQ_ARRAY                       <- '[' ']' / '[' PJQ_SCRIPT (',' PJQ_SCRIPT)* ']'
            PJQ_OBJECT                      <- '{' '}' / '{' PJQ_STRING ':' PJQ_SCRIPT (',' PJQ_STRING ':' PJQ_SCRIPT)* '}'
            PJQ_STRING                      <- < '"' (PJQ_ESCAPE_STRING_CHARACTER/PJQ_STRING_CHARACTER_DQUOTE)* '"' > / < "'" (PJQ_ESCAPE_STRING_CHARACTER/PJQ_STRING_CHARACTER_SQUOTE)* "'" >
            PJQ_ESCAPE_STRING_CHARACTER     <- < '\\' . >
            PJQ_STRING_CHARACTER_DQUOTE     <- < [^"] >
            PJQ_STRING_CHARACTER_SQUOTE     <- < [^'] >
            PJQ_NUMBER                      <- < [0-9]+[.][0-9]+ > / < [0-9]+ >
            PJQ_BOOLEAN                     <- PJQ_TRUE / PJQ_FALSE
            PJQ_TRUE                        <- 'true'
            PJQ_FALSE                       <- 'false'
            PJQ_NULL                        <- 'null'

            PJQ_BRACKET(TYPE)               <- '(' TYPE ')'

            %whitespace                     <- [ \n]*
        )")
    {
        if (!static_cast<bool>(parser)) {
            throw std::logic_error("JsonPath parser failed to create the parser.");
        }
        parser.enable_packrat_parsing();
    }

    std::shared_ptr<Peg::AbstractPrePegHandler> Engine::getHandler(const std::string& key)
    {
        auto it = handlers.find(key);
        if (handlers.end() == it) {
            return nullptr;
        }
        return it->second;
    }

    void Engine::setHandler(const std::string& key, std::shared_ptr<Peg::AbstractPrePegHandler> handler)
    {
        if (nullptr == handler || handler->getEngine().get() != this) {
            throw std::logic_error("Detached poco json query handler.");
        }

        handlers[key] = handler;
        parser[key.c_str()] = [handler](const peg::SemanticValues &vs) {
            auto ptr = handler->handle(vs);
            return PathFunction([ptr](std::shared_ptr<Scope>& scope) -> Poco::Dynamic::Var {
                return ptr->handle(scope);
            });
        };
    }

    void Engine::setOperatorHandler(const std::string& operation, std::shared_ptr<Operators::IOperator> operatorHandler)
    {
        if (nullptr == operatorHandler) {
            throw std::logic_error("nullptr for " + operation + " operation handler ptr.");
        }

        operatorHandlers[operation] = operatorHandler;
    }

    std::shared_ptr<Operators::IOperator> Engine::getOperatorHandler(const std::string& operation)
    {
        auto it = operatorHandlers.find(operation);
        if (operatorHandlers.end() == it) {
            return nullptr;
        }
        return it->second;
    }

    void Engine::setFilterHandler(const std::string& filter, std::shared_ptr<Filters::IFilter> filterHandler)
    {
        if (nullptr == filterHandler) {
            throw std::logic_error("nullptr for " + filter + " filter handler ptr.");
        }

        filterHandlers[filter] = filterHandler;
    }

    std::shared_ptr<Filters::IFilter> Engine::getFilterHandler(const std::string& filter)
    {
        auto it = filterHandlers.find(filter);
        if (filterHandlers.end() == it) {
            return nullptr;
        }
        return it->second;
    }

    Engine::PathFunction Engine::parse(const std::string& query)
    {
        PathFunction pathFunction;
        if (!parser.parse(query, pathFunction)) {
            throw std::logic_error("Unable to parse query: " + query);
        }
        return pathFunction;
    }
}