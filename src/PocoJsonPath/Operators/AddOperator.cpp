//
// Created by desrumaux on 11/10/2021.
//

#include "AddOperator.hpp"

namespace PocoJsonPath {
    namespace Operators {

        /***
         * Invoke an operator on string
         *
         * @param scope
         * @param leftMember
         * @param rightMember
         *
         * @return result
         */
        Poco::Dynamic::Var AddOperator::invokeString(const Scope& scope, std::string& leftMember, std::string& rightMember) const
        {
            return Poco::Dynamic::Var{leftMember + rightMember};
        }

        /***
         * Invoke an operator on number
         *
         * @param scope
         * @param leftMember
         * @param rightMember
         *
         * @return result
         */
        Poco::Dynamic::Var AddOperator::invokeNumber(const Scope& scope, double& leftMember, double& rightMember) const
        {
            return Poco::Dynamic::Var{leftMember + rightMember};
        }

        /***
         * Invoke an operator on object
         *
         * @param scope
         * @param leftMember
         * @param rightMember
         *
         * @return result
         */
        Poco::Dynamic::Var AddOperator::invokeArray(const Scope &scope, Poco::JSON::Array::Ptr &leftMember, Poco::JSON::Array::Ptr &rightMember) const
        {
            Poco::JSON::Array::Ptr result{new Poco::JSON::Array};
            for (auto& item : *leftMember) {
                result->add(item);
            }
            for (auto& item : *rightMember) {
                result->add(item);
            }
            return result;
        }
    }
}