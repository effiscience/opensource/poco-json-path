//
// Created by matthias on 07/12/2021.
//

#include "IsOperator.hpp"

#include "PocoJsonPath/Helpers/JsonHelper.hpp"

namespace PocoJsonPath {
    namespace Operators {

        /***
         * Invoke an operator
         *
         * @param scope
         * @param leftMember
         * @param rightMember
         *
         * @return result
         */
        Poco::Dynamic::Var IsOperator::invoke(const Scope& scope, Poco::Dynamic::Var& leftMember, Poco::Dynamic::Var& rightMember) const
        {
            if (!rightMember.isString()) {
                return false;
            }

            auto type = rightMember.extract<std::string>();
            if ("array" == type) {
                return Helpers::JsonHelper::castToJsonArray(leftMember).has_value();
            } else if ("object" == type) {
                return Helpers::JsonHelper::castToJsonObject(leftMember).has_value();
            } else if ("number" == type) {
                return leftMember.isNumeric();
            } else if ("string" == type) {
                return leftMember.isString();
            } else if ("null" == type) {
                return leftMember.isEmpty();
            } else if ("empty" == type) {
                if (leftMember.isEmpty()) {
                    return true;
                }

                auto mbArr = Helpers::JsonHelper::castToJsonArray(leftMember);
                if (mbArr.has_value() && mbArr.value()->size() > 0) {
                    return true;
                }

                auto mbDict = Helpers::JsonHelper::castToJsonObject(leftMember);
                if (mbDict.has_value() && mbDict.value()->size() > 0) {
                    return true;
                }

                return false;
            }

            return {};
        }

    }
}
