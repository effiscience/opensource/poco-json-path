//
// Created by matthias on 07/12/2021.
//

#ifndef POCOJSONPATH_ISOPERATOR_HPP
#define POCOJSONPATH_ISOPERATOR_HPP

#include "IOperator.hpp"

namespace PocoJsonPath {
    namespace Operators {
        class IsOperator : public IOperator {
        public:

            /***
             * Invoke an operator
             *
             * @param scope
             * @param leftMember
             * @param rightMember
             *
             * @return result
             */
            virtual Poco::Dynamic::Var invoke(const Scope& scope, Poco::Dynamic::Var& leftMember, Poco::Dynamic::Var& rightMember) const;

        };
    }
}


#endif //POCOJSONPATH_ISOPERATOR_HPP
