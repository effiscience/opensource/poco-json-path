//
// Created by matthias on 07/12/2021.
//

#ifndef POCOJSONPATH_INOPERATOR_HPP
#define POCOJSONPATH_INOPERATOR_HPP

#include "IOperator.hpp"

namespace PocoJsonPath {
    namespace Operators {
        class InOperator : public IOperator {
        public:

            /***
             * Invoke an operator
             *
             * @param scope
             * @param leftMember
             * @param rightMember
             *
             * @return result
             */
            virtual Poco::Dynamic::Var invoke(const Scope& scope, Poco::Dynamic::Var& leftMember, Poco::Dynamic::Var& rightMember) const;

        };
    }
}

#endif //POCOJSONPATH_INOPERATOR_HPP
