//
// Created by desrumaux on 11/10/2021.
//

#ifndef POCOJSONPATH_ADDOPERATOR_HPP
#define POCOJSONPATH_ADDOPERATOR_HPP

#include "AbstractOperator.hpp"

namespace PocoJsonPath {
    namespace Operators {

        class AddOperator : public AbstractOperator {
        public:

            /***
             * Invoke an operator on string
             *
             * @param scope
             * @param leftMember
             * @param rightMember
             *
             * @return result
             */
            virtual Poco::Dynamic::Var invokeString(const Scope& scope, std::string& leftMember, std::string& rightMember) const;

            /***
             * Invoke an operator on number
             *
             * @param scope
             * @param leftMember
             * @param rightMember
             *
             * @return result
             */
            virtual Poco::Dynamic::Var invokeNumber(const Scope& scope, double& leftMember, double& rightMember) const;

            /***
             * Invoke an operator on object
             *
             * @param scope
             * @param leftMember
             * @param rightMember
             *
             * @return result
             */
            virtual Poco::Dynamic::Var invokeArray(const Scope &scope, Poco::JSON::Array::Ptr &leftMember, Poco::JSON::Array::Ptr &rightMember) const;
        };

    }
}

#endif //POCOJSONPATH_ADDOPERATOR_HPP
