//
// Created by matthias on 07/12/2021.
//

#include "InOperator.hpp"

#include "PocoJsonPath/Engine.hpp"
#include "PocoJsonPath/Helpers/JsonHelper.hpp"
#include "PocoJsonPath/Helpers/StringsHelper.hpp"

#include "EqualOperator.hpp"

namespace PocoJsonPath {
    namespace Operators {

        Poco::Dynamic::Var InOperator::invoke(const Scope& scope, Poco::Dynamic::Var& leftMember, Poco::Dynamic::Var& rightMember) const
        {
            auto mbDict = Helpers::JsonHelper::castToJsonObject(rightMember);
            if (mbDict.has_value()) {
                if (!leftMember.isString()) {
                    return false;
                }

                return (*mbDict)->has(leftMember.extract<std::string>());
            }

            auto mbArr = Helpers::JsonHelper::castToJsonArray(rightMember);
            if (mbArr.has_value()) {
                EqualOperator eq;
                auto arr = *mbArr;
                for (int i = 0; i < arr->size(); i++) {
                    auto item = arr->get(i);
                    if (eq.invoke(scope, leftMember, item)) {
                        return true;
                    }
                }

                return false;
            }

            if (leftMember.isString() && rightMember.isString()) {
                return PocoJsonPath::Helpers::StringsHelper::contains(
                        leftMember.extract<std::string>(),
                        rightMember.extract<std::string>());
            }

            return false;
        }

    }
}