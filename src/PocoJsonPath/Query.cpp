//
// Created by desrumaux on 23/11/2021.
//

#include "Query.hpp"
#include "Scope.hpp"

namespace PocoJsonPath {

    Query::Query(const std::string& query)
        : Query(query, Engine::New())
    {}

    Query::Query(const std::string& query, std::shared_ptr<Engine> engine)
        : engine(engine)
        , query(query)
        , function(engine->parse(query))
        , userVariables()
    {}

    Poco::Dynamic::Var Query::run(Poco::Dynamic::Var variable)
    {
        auto root = Scope::NewRootScope(engine, variable);
        root->setVariables(userVariables);
        return function(root);
    }

    std::shared_ptr<Engine> Query::getEngine()
    {
        return {this->engine};
    }

    void Query::setUserVariables(std::map<std::string, Poco::Dynamic::Var> userVariables)
    {
        this->userVariables = userVariables;
    }

    std::map<std::string, Poco::Dynamic::Var> Query::getUserVariables()
    {
        return userVariables;
    }

    void Query::addUserVariable(std::string key, Poco::Dynamic::Var userVariable)
    {
        userVariables[key] = userVariable;
    }

    void Query::removeUserVariable(std::string key)
    {
        auto it = userVariables.find(key);
        if (userVariables.end() == it) {
            return;
        }
        userVariables.erase(it);
    }
}
