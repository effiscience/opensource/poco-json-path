//
// Created by desrumaux on 30/11/2021.
//

#ifndef POCOJSONPATH_KEYWORD_HPP
#define POCOJSONPATH_KEYWORD_HPP

namespace PocoJsonPath {
    namespace Keyword {
        #define _(KEYWORD) constexpr const char KEYWORD[] = #KEYWORD
        _(PJQ_STRING);
        _(PJQ_NUMBER);
        _(PJQ_TRUE);
        _(PJQ_FALSE);
        _(PJQ_OBJECT);
        _(PJQ_ARRAY);
        _(PJQ_KEYWORD);
        _(PJQ_NUMBER_KEYWORD);
        _(PJQ_INDEX);
        _(PJQ_DYNAMIC_INDEX);
        _(PJQ_CURRENT);
        _(PJQ_ROOT);
        _(PJQ_QUERY);
        _(PJQ_PIPE);
        _(PJQ_FILTER);
        _(PJQ_SCRIPT);
        _(PJQ_SCRIPT_LOGIC_1);
        _(PJQ_SCRIPT_LOGIC_1_OPERATOR);
        _(PJQ_SCRIPT_LOGIC_IS_OPERATOR);
        _(PJQ_SCRIPT_LOGIC_2);
        _(PJQ_SCRIPT_LOGIC_2_OPERATOR);
        _(PJQ_SCRIPT_MATH_1);
        _(PJQ_SCRIPT_MATH_1_OPERATOR);
        _(PJQ_SCRIPT_MATH_2);
        _(PJQ_SCRIPT_MATH_2_OPERATOR);
        _(PJQ_SCOPED_VARIABLE);
        _(PJQ_USER_VARIABLE);
        _(PJQ_RECURSIVE_INDEX);
        _(PJQ_RECURSIVE_FILTER);
        #undef _

        constexpr const char PJQ_KEYWORD_NULL[] = "PJQ_NULL";
    }
}

#endif //POCOJSONPATH_KEYWORD_HPP
