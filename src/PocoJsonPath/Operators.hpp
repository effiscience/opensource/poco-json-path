//
// Created by matthias on 07/12/2021.
//

#ifndef POCOJSONPATH_OPERATORS_HPP
#define POCOJSONPATH_OPERATORS_HPP

namespace PocoJsonPath {
    namespace Operators {
        #define _(KEYWORD, VALUE) constexpr const char KEYWORD[] = #VALUE
        _(ADD, +);
        _(SUBSTRACT, -);
        _(MULT, *);
        _(DIVIDE, /);
        _(AND, &&);
        _(OR, ||);
        _(EQUAL, ==);
        _(NOT_EQUAL, !=);
        _(GREATER_THAN, >);
        _(GREATER_THAN_OR_EQUAL, >=);
        _(LOWER_THAN, <);
        _(LOWER_THAN_OR_EQUAL, <=);
        _(IS, is);
        _(IN, in);
        #undef _
    }
}

#endif //POCOJSONPATH_OPERATORS_HPP
