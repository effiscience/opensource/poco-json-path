//
// Created by matthias on 21/12/2021.
//

#ifndef POCOJSONPATH_FILTERS_HPP
#define POCOJSONPATH_FILTERS_HPP

namespace PocoJsonPath {
    namespace Filters {
        #define _(FILTERS) constexpr const char FILTERS[] = #FILTERS
        _(stringify);
        _(parse);
        _(quote);
        _(unquote);
        #undef _
    }
}

#endif //POCOJSONPATH_FILTERS_HPP
