//
// Created by desrumaux on 23/11/2021.
//

#ifndef POCOJSONPATH_QUERY_HPP
#define POCOJSONPATH_QUERY_HPP

#include <Poco/Dynamic/Var.h>

#include "Engine.hpp"

namespace PocoJsonPath {

    class Query {
    private:

        std::shared_ptr<Engine> engine;

        std::string query;

        Engine::PathFunction function;

        std::map<std::string, Poco::Dynamic::Var> userVariables;

    public:

        Query(const std::string& query);

        Query(const std::string& query, std::shared_ptr<Engine> engine);

        Poco::Dynamic::Var run(Poco::Dynamic::Var variable);

        std::shared_ptr<Engine> getEngine();

        void setUserVariables(std::map<std::string, Poco::Dynamic::Var> userVariables);

        std::map<std::string, Poco::Dynamic::Var> getUserVariables();

        void addUserVariable(std::string key, Poco::Dynamic::Var userVariable);

        void removeUserVariable(std::string key);
    };

}


#endif //POCOJSONPATH_QUERY_HPP
