//
// Created by desrumaux on 23/11/2021.
//

#ifndef POCOJSONPATH_ENGINE_HPP
#define POCOJSONPATH_ENGINE_HPP

#include <functional>
#include <map>
#include <memory>

#include <peglib.h>
#include <Poco/Dynamic/Var.h>

namespace PocoJsonPath {

    class Scope;

    namespace Peg {
        class AbstractPrePegHandler;
    }

    namespace Operators {
        class IOperator;
    }

    namespace Filters {
        class IFilter;
    }

    class Engine {
    public:

        using PathFunction = std::function<Poco::Dynamic::Var(std::shared_ptr<Scope>&)>;

        static std::shared_ptr<Engine> New();

    private:

        peg::parser parser;

        std::map<std::string, std::shared_ptr<Peg::AbstractPrePegHandler>> handlers;

        std::map<std::string, std::shared_ptr<Operators::IOperator>> operatorHandlers;

        std::map<std::string, std::shared_ptr<Filters::IFilter>> filterHandlers;

    public:

        Engine();

        Engine(const Engine& other) = delete;

        std::shared_ptr<Peg::AbstractPrePegHandler> getHandler(const std::string& key);

        void setHandler(const std::string& key, std::shared_ptr<Peg::AbstractPrePegHandler> handler);

        void setOperatorHandler(const std::string& operation, std::shared_ptr<Operators::IOperator> operatorHandler);

        std::shared_ptr<Operators::IOperator> getOperatorHandler(const std::string& operation);

        void setFilterHandler(const std::string& filter, std::shared_ptr<Filters::IFilter> filterHandler);

        std::shared_ptr<Filters::IFilter> getFilterHandler(const std::string& filter);

        PathFunction parse(const std::string& query);
    };
}



#endif //POCOJSONPATH_ENGINE_HPP
