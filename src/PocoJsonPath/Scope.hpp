//
// Created by desrumaux on 23/11/2021.
//

#ifndef POCOJSONPATH_SCOPE_HPP
#define POCOJSONPATH_SCOPE_HPP

#include <Poco/Dynamic/Var.h>
#include <vector>

#include "Engine.hpp"

namespace PocoJsonPath {

    class Scope {
    public:

        static std::shared_ptr<Scope> NewRootScope(std::shared_ptr<Engine> engine, Poco::Dynamic::Var variable);

        static std::shared_ptr<Scope> NewScope(std::shared_ptr<Scope> parent, Poco::Dynamic::Var variable);

    private:

        std::shared_ptr<Engine> engine;

        std::shared_ptr<Scope> root;

        std::shared_ptr<Scope> parent;

        Poco::Dynamic::Var value;

        std::vector<std::shared_ptr<Scope>> children;

        std::map<std::string, Poco::Dynamic::Var> variables;

    public:

        Scope(std::shared_ptr<Engine> engine, std::shared_ptr<Scope> root, std::shared_ptr<Scope> parent, Poco::Dynamic::Var value);

        Scope(const Scope&) = delete;

        bool isRootScope() const;

        std::shared_ptr<Engine> getEngine();

        std::shared_ptr<Scope> getRoot();

        std::shared_ptr<Scope> getParent();

        Poco::Dynamic::Var getValue();

        void setValue(Poco::Dynamic::Var value);

        std::map<std::string, Poco::Dynamic::Var> getVariables();

        void setVariables(std::map<std::string, Poco::Dynamic::Var> variables);

        bool hasVariable(std::string name) const;

        Poco::Dynamic::Var getVariable(std::string name);

        void setVariable(std::string name, Poco::Dynamic::Var variable);

        void deleteVariable(std::string name);
    };

}


#endif //POCOJSONPATH_SCOPE_HPP
