//
// Created by matthias on 07/12/2021.
//

#ifndef POCOJSONPATH_VARIABLES_HPP
#define POCOJSONPATH_VARIABLES_HPP

namespace PocoJsonPath {
    namespace Variables {
        #define _(VARIABLES) constexpr const char VARIABLES[] = #VARIABLES
        _(length);
        _(index);
        #undef _
    }
}

#endif //POCOJSONPATH_VARIABLES_HPP
