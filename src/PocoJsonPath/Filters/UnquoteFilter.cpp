//
// Created by matthias on 21/12/2021.
//

#include "UnquoteFilter.hpp"

#include "PocoJsonPath/Helpers/StringsHelper.hpp"

namespace PocoJsonPath {
    namespace Filters {

        /**
         * Filter an subject
         *
         * @param subject
         * @param parameters
         *
         * @return result
         */
        Poco::Dynamic::Var UnquoteFilter::invoke(std::shared_ptr<Engine> engine, Poco::Dynamic::Var subject, std::vector<Poco::Dynamic::Var>& parameters) const
        {
            if (!subject.isString()) {
                return {};
            }

            bool doubleQuote = false;
            if (!parameters.empty() && parameters.front().isBoolean()) {
                doubleQuote = parameters.front().extract<bool>();
            }

            const auto& string = subject.extract<std::string>();
            if (doubleQuote) {
                return Helpers::StringsHelper::undoubleQuote(string);
            } else {
                return Helpers::StringsHelper::unquote(string);
            }
        }

    }
}