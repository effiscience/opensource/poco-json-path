//
// Created by matthias on 21/12/2021.
//

#ifndef POCOJSONPATH_QUOTEFILTER_HPP
#define POCOJSONPATH_QUOTEFILTER_HPP

#include "IFilter.hpp"

namespace PocoJsonPath {
    namespace Filters {

        class QuoteFilter : public IFilter {
        public:

            /**
             * Filter an subject
             *
             * @param subject
             * @param parameters
             *
             * @return result
             */
            virtual Poco::Dynamic::Var invoke(std::shared_ptr<Engine> engine, Poco::Dynamic::Var subject, std::vector<Poco::Dynamic::Var>& parameters) const;

        };

    }
}


#endif //POCOJSONPATH_QUOTEFILTER_HPP
