//
// Created by matthias on 21/12/2021.
//

#include "ParseFilter.hpp"

#include <Poco/JSON/Parser.h>

namespace PocoJsonPath {
    namespace Filters {

        /**
         * Filter an subject
         *
         * @param subject
         * @param parameters
         *
         * @return result
         */
        Poco::Dynamic::Var ParseFilter::invoke(std::shared_ptr<Engine> engine, Poco::Dynamic::Var subject, std::vector<Poco::Dynamic::Var>& parameters) const
        {
            if (!subject.isString()) {
                return {};
            }

            try {
                Poco::JSON::Parser parser;
                return parser.parse(subject.extract<std::string>());
            } catch (const std::exception&) {
                return {};
            }
        }

    }
}