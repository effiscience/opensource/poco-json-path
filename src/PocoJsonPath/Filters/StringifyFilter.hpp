//
// Created by matthias on 21/12/2021.
//

#ifndef POCOJSONPATH_STRINGIFYFILTER_HPP
#define POCOJSONPATH_STRINGIFYFILTER_HPP

#include "IFilter.hpp"

namespace PocoJsonPath {
    namespace Filters {

        class StringifyFilter : public IFilter {
        public:

            /**
             * Filter an subject
             *
             * @param subject
             * @param parameters
             *
             * @return result
             */
            virtual Poco::Dynamic::Var invoke(std::shared_ptr<Engine> engine, Poco::Dynamic::Var subject, std::vector<Poco::Dynamic::Var>& parameters) const;

        };

    }
}


#endif //POCOJSONPATH_STRINGIFYFILTER_HPP
