//
// Created by matthias on 21/12/2021.
//

#include "StringifyFilter.hpp"

#include <Poco/JSON/Stringifier.h>

#include "PocoJsonPath/Helpers/JsonHelper.hpp"

namespace PocoJsonPath {
    namespace Filters {

        /**
         * Filter an subject
         *
         * @param subject
         * @param parameters
         *
         * @return result
         */
        Poco::Dynamic::Var StringifyFilter::invoke(std::shared_ptr<Engine> engine, Poco::Dynamic::Var subject, std::vector<Poco::Dynamic::Var>& parameters) const
        {
            std::ostringstream oss;
            long indent = 0;
            if (!parameters.empty()) {
                auto mbIndent = PocoJsonPath::Helpers::JsonHelper::castToDouble(parameters.front());
                if (mbIndent.has_value()) {
                    indent = mbIndent.value();
                }
            }
            Poco::JSON::Stringifier::stringify(subject, oss, indent);
            return oss.str();
        }

    }
}