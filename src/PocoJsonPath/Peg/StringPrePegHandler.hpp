//
// Created by desrumaux on 30/11/2021.
//

#ifndef POCOJSONPATH_STRINGPREPEGHANDLER_HPP
#define POCOJSONPATH_STRINGPREPEGHANDLER_HPP

#include "AbstractPrePegHandler.hpp"

namespace PocoJsonPath {
    namespace Peg {

        class StringPrePegHandler : public AbstractPrePegHandler {
        public:

            StringPrePegHandler(std::shared_ptr<Engine> engine);

            virtual std::shared_ptr<AbstractPegHandler> handle(const peg::SemanticValues& sv);

        };

    }
}


#endif //POCOJSONPATH_STRINGPREPEGHANDLER_HPP
