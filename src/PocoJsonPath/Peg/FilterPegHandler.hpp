//
// Created by matthias on 07/12/2021.
//

#ifndef POCOJSONPATH_FILTERPEGHANDLER_HPP
#define POCOJSONPATH_FILTERPEGHANDLER_HPP

#include "AbstractPegHandler.hpp"

namespace PocoJsonPath {
    namespace Peg {

        class FilterPegHandler : public AbstractPegHandler {
        protected:

            Engine::PathFunction filter;

        public:

            FilterPegHandler(std::shared_ptr<Engine> engine, Engine::PathFunction filter);

            virtual Poco::Dynamic::Var handle(std::shared_ptr<Scope> scope);

        };

    }
}

#endif //POCOJSONPATH_FILTERPEGHANDLER_HPP
