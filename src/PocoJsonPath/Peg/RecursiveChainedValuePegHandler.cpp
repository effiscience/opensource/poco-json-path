//
// Created by matthias on 10/12/2021.
//

#include "RecursiveChainedValuePegHandler.hpp"

#include "PocoJsonPath/Helpers/JsonHelper.hpp"
#include "PocoJsonPath/Variables.hpp"

namespace PocoJsonPath {
    namespace Peg {

        RecursiveChainedValuePegHandler::RecursiveChainedValuePegHandler(std::shared_ptr<Engine> engine, Engine::PathFunction source, std::vector<Engine::PathFunction> chain, bool isFilter)
            : AbstractPegHandler(engine)
            , source(source)
            , chain(chain)
            , isFilter(isFilter)
        {}

        void RecursiveChainedValuePegHandler::evaluate(Poco::JSON::Array::Ptr& output, std::shared_ptr<Scope> recursionRootScope, std::shared_ptr<Scope> scope, std::shared_ptr<Scope> child, bool last)
        {
            auto value = source(child);
            if (value.isEmpty()) {
                if (!last) {
                    iterate(output, recursionRootScope, child);
                }
            } else {
                if (isFilter) {
                    auto mbDict = Helpers::JsonHelper::castToJsonObject(value);
                    if (mbDict.has_value()) {
                        auto dict = *mbDict;
                        if (dict->size() == 0) {
                            return iterate(output, recursionRootScope, child);
                        }

                        for (auto name : dict->getNames()) {
                            auto resultItemScope = Scope::NewScope(child, dict->get(name));
                            for (auto filter : chain) {
                                resultItemScope = Scope::NewScope(resultItemScope, filter(resultItemScope));
                            }
                            output->add(resultItemScope->getValue());
                        }

                        // Try the other one with recursion
                        auto origin = child->getValue();
                        auto originalDict = *Helpers::JsonHelper::castToJsonObject(origin);
                        for (auto name : originalDict->getNames()) {
                            if (!dict->has(name)) {
                                auto otherItemScope = Scope::NewScope(child, originalDict->get(name));
                                iterate(output, recursionRootScope, otherItemScope);
                            }
                        }

                        return;
                    }

                    auto mbArray = Helpers::JsonHelper::castToJsonArray(value);
                    if (mbArray.has_value() && (*mbArray)->size() == 0) {
                        auto array = *mbArray;
                        if (array->size() == 0) {
                            return iterate(output, recursionRootScope, child);
                        }

                        for (auto item : *array) {
                            auto resultItemScope = Scope::NewScope(child, item);
                            for (auto filter : chain) {
                                resultItemScope = Scope::NewScope(resultItemScope, filter(resultItemScope));
                            }
                            output->add(resultItemScope->getValue());
                        }

                        return;
                    }

                    auto resultItemScope = Scope::NewScope(child, value);
                    for (auto filter : chain) {
                        resultItemScope = Scope::NewScope(resultItemScope, filter(resultItemScope));
                    }
                    output->add(resultItemScope->getValue());
                } else {
                    auto resultItemScope = Scope::NewScope(child, value);
                    for (auto filter : chain) {
                        resultItemScope = Scope::NewScope(resultItemScope, filter(resultItemScope));
                    }
                    output->add(resultItemScope->getValue());
                }
            }
        }

        void RecursiveChainedValuePegHandler::iterate(Poco::JSON::Array::Ptr& output, std::shared_ptr<Scope> recursionRootScope, std::shared_ptr<Scope> scope)
        {
            auto value = scope->getValue();

            auto mbDict = Helpers::JsonHelper::castToJsonObject(value);
            if (mbDict.has_value()) {
                auto dict = *mbDict;
                for (auto name : dict->getNames()) {
                    auto child = Scope::NewScope(scope, dict->get(name));
                    child->setVariable(Variables::index, name);
                    child->setVariable(Variables::length, dict->size());
                    evaluate(output, recursionRootScope, scope, child);
                }
                return;
            }

            auto mbArray = Helpers::JsonHelper::castToJsonArray(value);
            if (mbArray.has_value()) {
                auto array = *mbArray;
                for (size_t i = 0; i < array->size(); i++) {
                    auto child = Scope::NewScope(scope, array->get(i));
                    child->setVariable(Variables::index, i);
                    child->setVariable(Variables::length, array->size());
                    evaluate(output, recursionRootScope, scope, child);
                }
                return;
            }

            evaluate(output, recursionRootScope, scope, scope, true);
        }

        Poco::Dynamic::Var RecursiveChainedValuePegHandler::handle(std::shared_ptr<Scope> scope)
        {
            Poco::JSON::Array::Ptr result{new Poco::JSON::Array};
            evaluate(result, scope, scope, scope);
            return result;
        }

    }
}