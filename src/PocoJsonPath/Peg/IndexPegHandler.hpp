//
// Created by desrumaux on 30/11/2021.
//

#ifndef POCOJSONPATH_INDEXPEGHANDLER_HPP
#define POCOJSONPATH_INDEXPEGHANDLER_HPP

#include "AbstractPegHandler.hpp"

namespace PocoJsonPath {
    namespace Peg {

        class IndexPegHandler : public AbstractPegHandler {
        protected:

            Engine::PathFunction keyword;

        public:

            IndexPegHandler(std::shared_ptr<Engine> engine, Engine::PathFunction keyword);

            virtual Poco::Dynamic::Var handle(std::shared_ptr<Scope> scope);

        };

    }
}


#endif //POCOJSONPATH_INDEXPEGHANDLER_HPP
