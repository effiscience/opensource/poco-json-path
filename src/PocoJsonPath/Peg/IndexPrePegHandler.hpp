//
// Created by desrumaux on 30/11/2021.
//

#ifndef POCOJSONPATH_INDEXPREPEGHANDLER_HPP
#define POCOJSONPATH_INDEXPREPEGHANDLER_HPP

#include "AbstractPrePegHandler.hpp"

namespace PocoJsonPath {
    namespace Peg {

        class IndexPrePegHandler : public AbstractPrePegHandler {
        public:

            IndexPrePegHandler(std::shared_ptr<Engine> engine);

            virtual std::shared_ptr<AbstractPegHandler> handle(const peg::SemanticValues& sv);

        };

    }
}

#endif //POCOJSONPATH_INDEXPREPEGHANDLER_HPP
