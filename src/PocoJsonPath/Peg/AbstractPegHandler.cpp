//
// Created by desrumaux on 23/11/2021.
//

#include "AbstractPegHandler.hpp"

namespace PocoJsonPath {
    namespace Peg {

        AbstractPegHandler::AbstractPegHandler(std::shared_ptr<Engine> engine)
            : engine(engine)
        {}

        std::shared_ptr<Engine> AbstractPegHandler::getEngine()
        {
            return engine;
        }

    }
}
