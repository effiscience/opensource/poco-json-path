//
// Created by desrumaux on 30/11/2021.
//

#include "NumberPrePegHandler.hpp"

#include "ConstantePegHandler.hpp"

namespace PocoJsonPath {
    namespace Peg {

        NumberPrePegHandler::NumberPrePegHandler(std::shared_ptr<Engine> engine)
            : AbstractPrePegHandler(engine)
        {}

        std::shared_ptr<AbstractPegHandler> NumberPrePegHandler::handle(const peg::SemanticValues& sv)
        {
            auto number = std::stod(sv.token_to_string());
            return std::make_shared<ConstantePegHandler>(engine, number);
        }

    }
}
