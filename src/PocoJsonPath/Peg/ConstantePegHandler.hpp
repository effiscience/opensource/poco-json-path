//
// Created by desrumaux on 30/11/2021.
//

#ifndef POCOJSONPATH_CONSTANTEPEGHANDLER_HPP
#define POCOJSONPATH_CONSTANTEPEGHANDLER_HPP

#include "AbstractPegHandler.hpp"

namespace PocoJsonPath {
    namespace Peg {

        class ConstantePegHandler : public AbstractPegHandler {
        protected:

            Poco::Dynamic::Var value;

        public:

            ConstantePegHandler(std::shared_ptr<Engine> engine, Poco::Dynamic::Var value);

            virtual Poco::Dynamic::Var handle(std::shared_ptr<Scope> scope);

        };

    }
}


#endif //POCOJSONPATH_CONSTANTEPEGHANDLER_HPP
