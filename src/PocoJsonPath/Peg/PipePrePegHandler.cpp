//
// Created by matthias on 07/12/2021.
//

#include "PipePrePegHandler.hpp"

#include "PipePegHandler.hpp"

namespace PocoJsonPath {
    namespace Peg {

        PipePrePegHandler::PipePrePegHandler(std::shared_ptr<Engine> engine)
            : AbstractPrePegHandler(engine)
        {}

        std::shared_ptr<AbstractPegHandler> PipePrePegHandler::handle(const peg::SemanticValues& sv)
        {
            auto pipeName = std::any_cast<Engine::PathFunction>(sv[0]);
            std::vector<Engine::PathFunction> variables;
            for (size_t i = 1; i < sv.size(); i++) {
                variables.push_back(std::any_cast<Engine::PathFunction>(sv[i]));
            }
            return std::make_shared<PipePegHandler>(engine, pipeName, variables);
        }

    }
}
