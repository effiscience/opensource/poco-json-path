//
// Created by desrumaux on 30/11/2021.
//

#include "RootPegHandler.hpp"

namespace PocoJsonPath {
    namespace Peg {

        RootPegHandler::RootPegHandler(std::shared_ptr<Engine> engine)
            : AbstractPegHandler(engine)
        {}

        Poco::Dynamic::Var RootPegHandler::handle(std::shared_ptr<Scope> scope)
        {
            return scope->isRootScope()
                ? scope->getValue()
                : scope->getRoot()->getValue();
        }

    }
}
