//
// Created by matthias on 10/12/2021.
//

#ifndef POCOJSONPATH_RECURSIVECHAINEDVALUEPEGHANDLER_HPP
#define POCOJSONPATH_RECURSIVECHAINEDVALUEPEGHANDLER_HPP

#include "AbstractPegHandler.hpp"

#include <Poco/JSON/Array.h>

namespace PocoJsonPath {
    namespace Peg {

        class RecursiveChainedValuePegHandler : public AbstractPegHandler {
        private:

            Engine::PathFunction source;

            std::vector<Engine::PathFunction> chain;

            bool isFilter;

        public:

            RecursiveChainedValuePegHandler(std::shared_ptr<Engine> engine, Engine::PathFunction source, std::vector<Engine::PathFunction> chain, bool isFilter);

        private:

            virtual void evaluate(Poco::JSON::Array::Ptr& output, std::shared_ptr<Scope> recursionRootScope, std::shared_ptr<Scope> scope, std::shared_ptr<Scope> child, bool last = false);

            virtual void iterate(Poco::JSON::Array::Ptr& output, std::shared_ptr<Scope> recursionRootScope, std::shared_ptr<Scope> scope);

        public:

            virtual Poco::Dynamic::Var handle(std::shared_ptr<Scope> scope);

        };

    }
}


#endif //POCOJSONPATH_RECURSIVECHAINEDVALUEPEGHANDLER_HPP
