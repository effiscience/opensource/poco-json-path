//
// Created by matthias on 07/12/2021.
//

#ifndef POCOJSONPATH_VARIABLEPEGHANDLER_HPP
#define POCOJSONPATH_VARIABLEPEGHANDLER_HPP

#include "AbstractPrePegHandler.hpp"

namespace PocoJsonPath {
    namespace Peg {

        class VariablePegHandler : public AbstractPegHandler {

            Engine::PathFunction key;
            bool root;

        public:

            VariablePegHandler(std::shared_ptr<Engine> engine, Engine::PathFunction key, bool root = false);

            virtual Poco::Dynamic::Var handle(std::shared_ptr<Scope> scope);

        };

    }
}

#endif //POCOJSONPATH_VARIABLEPEGHANDLER_HPP
