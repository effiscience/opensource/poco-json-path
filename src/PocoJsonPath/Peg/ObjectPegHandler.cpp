//
// Created by matthias on 20/12/2021.
//

#include "ObjectPegHandler.hpp"

#include <Poco/JSON/Object.h>

namespace PocoJsonPath {
    namespace Peg {

        ObjectPegHandler::ObjectPegHandler(std::shared_ptr<Engine> engine, std::vector<std::pair<Engine::PathFunction, Engine::PathFunction>> values)
            : AbstractPegHandler(engine)
            , values(values)
        {}

        Poco::Dynamic::Var ObjectPegHandler::handle(std::shared_ptr<Scope> scope)
        {
            Poco::JSON::Object::Ptr result{new Poco::JSON::Object};
            for(auto& item : values) {
                result->set(item.first(scope).extract<std::string>(), item.second(scope));
            }
            return result;
        }

    }
}
