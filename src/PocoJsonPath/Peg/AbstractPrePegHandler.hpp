//
// Created by desrumaux on 30/11/2021.
//

#ifndef POCOJSONPATH_ABSTRACTPREPEGHANDLER_HPP
#define POCOJSONPATH_ABSTRACTPREPEGHANDLER_HPP

#include "AbstractPegHandler.hpp"

namespace PocoJsonPath {
    namespace Peg {

        class AbstractPrePegHandler {
        protected:

            std::shared_ptr<Engine> engine;

        public:

            AbstractPrePegHandler(std::shared_ptr<Engine> engine);

            std::shared_ptr<Engine> getEngine();

            virtual std::shared_ptr<AbstractPegHandler> handle(const peg::SemanticValues& sv) = 0;
        };

    }
}

#endif //POCOJSONPATH_ABSTRACTPREPEGHANDLER_HPP
