//
// Created by matthias on 13/12/2021.
//

#include "RecursiveChainedValuePrePegHandler.hpp"

#include "RecursiveChainedValuePegHandler.hpp"

namespace PocoJsonPath {
    namespace Peg {

        RecursiveChainedValuePrePegHandler::RecursiveChainedValuePrePegHandler(std::shared_ptr<Engine> engine, bool isFilter)
                : AbstractPrePegHandler(engine)
                , isFilter(isFilter)
        {}

        std::shared_ptr<AbstractPegHandler> RecursiveChainedValuePrePegHandler::handle(const peg::SemanticValues& sv)
        {
            auto source = std::any_cast<Engine::PathFunction>(sv[0]);
            std::vector<Engine::PathFunction> chain;

            for (int i = 1; i < sv.size(); i++) {
                chain.push_back(std::any_cast<Engine::PathFunction>(sv[i]));
            }

            return std::make_shared<RecursiveChainedValuePegHandler>(engine, source, chain, isFilter);
        }

    }
}
