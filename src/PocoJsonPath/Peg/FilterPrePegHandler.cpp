//
// Created by matthias on 07/12/2021.
//

#include "FilterPrePegHandler.hpp"

#include "FilterPegHandler.hpp"

namespace PocoJsonPath {
    namespace Peg {

        FilterPrePegHandler::FilterPrePegHandler(std::shared_ptr<Engine> engine)
            : AbstractPrePegHandler(engine)
        {}

        std::shared_ptr<AbstractPegHandler> FilterPrePegHandler::handle(const peg::SemanticValues& sv)
        {
            return std::make_shared<FilterPegHandler>(engine, std::any_cast<Engine::PathFunction>(sv[0]));
        }

    }
}