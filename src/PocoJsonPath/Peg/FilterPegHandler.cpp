//
// Created by matthias on 07/12/2021.
//

#include "FilterPegHandler.hpp"

#include "PocoJsonPath/Helpers/JsonHelper.hpp"
#include "PocoJsonPath/Variables.hpp"

namespace PocoJsonPath {
    namespace Peg {

        FilterPegHandler::FilterPegHandler(std::shared_ptr<Engine> engine, Engine::PathFunction filter)
            : AbstractPegHandler(engine)
            , filter(filter)
        {}

        Poco::Dynamic::Var FilterPegHandler::handle(std::shared_ptr<Scope> scope)
        {
            auto item = scope->getValue();
            auto mbDict = Helpers::JsonHelper::castToJsonObject(item);
            auto mbArr = Helpers::JsonHelper::castToJsonArray(item);

            std::vector<std::string> variablesKeys;
            #define _(key, value) \
                variablesKeys.push_back(key); \
                scope->setVariable(key, value)

            if (mbDict.has_value()) {
                auto dict = *mbDict;
                _(Variables::length, dict->size());
            } else if (mbArr.has_value()) {
                auto dict = *mbArr;
                _(Variables::length, dict->size());
            }

            #undef _

            if (mbDict.has_value()) {
                Poco::JSON::Object::Ptr result{new Poco::JSON::Object{}};
                auto dict = *mbDict;

                for (auto key : dict->getNames()) {
                    auto childScope = Scope::NewScope(scope, dict->get(key));
                    childScope->setVariable(Variables::index, key);
                    auto filterResult = filter(childScope);
                    if (filterResult.isEmpty() || (
                            filterResult.isBoolean()
                            && !filterResult.extract<bool>()
                            )) {
                        continue;
                    }
                    result->set(key, childScope->getValue());
                }

                for (auto& key : variablesKeys) {
                    scope->deleteVariable(key);
                }

                return result;
            }

            if (mbArr.has_value()) {
                Poco::JSON::Array::Ptr result{new Poco::JSON::Array{}};
                auto arr = *mbArr;

                for (size_t i = 0; i < arr->size(); i++) {
                    auto childScope = Scope::NewScope(scope, arr->get(i));
                    childScope->setVariable(Variables::index, i);
                    auto filterResult = filter(childScope);
                    if (filterResult.isEmpty() || (
                            filterResult.isBoolean()
                            && !filterResult.extract<bool>()
                    )) {
                        continue;
                    }
                    result->add(childScope->getValue());
                }

                for (auto& key : variablesKeys) {
                    scope->deleteVariable(key);
                }

                return result;
            }

            Poco::Dynamic::Var result;
            auto filterResult = filter(scope);
            if (filterResult.isEmpty() || (
                    filterResult.isBoolean()
                    && !filterResult.extract<bool>()
            )) {
                result = {};
            } else {
                result = scope->getValue();
            }

            for (auto& key : variablesKeys) {
                scope->deleteVariable(key);
            }

            return result;
        }

    }
}