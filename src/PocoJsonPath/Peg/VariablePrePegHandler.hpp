//
// Created by matthias on 07/12/2021.
//

#ifndef POCOJSONPATH_VARIABLEPREPEGHANDLER_HPP
#define POCOJSONPATH_VARIABLEPREPEGHANDLER_HPP

#include "AbstractPrePegHandler.hpp"

namespace PocoJsonPath {
    namespace Peg {

        class VariablePrePegHandler : public AbstractPrePegHandler {
            bool root;

        public:

            VariablePrePegHandler(std::shared_ptr<Engine> engine, bool root = false);

            virtual std::shared_ptr<AbstractPegHandler> handle(const peg::SemanticValues& sv);

        };

    }
}

#endif //POCOJSONPATH_VARIABLEPREPEGHANDLER_HPP
