//
// Created by matthias on 07/12/2021.
//

#include "ChainedValuePegHandler.hpp"

namespace PocoJsonPath {
    namespace Peg {

        ChainedValuePegHandler::ChainedValuePegHandler(std::shared_ptr<Engine> engine, Engine::PathFunction source, std::vector<Engine::PathFunction> chain)
            : AbstractPegHandler(engine)
            , source(source)
            , chain(chain)
        {}

        Poco::Dynamic::Var ChainedValuePegHandler::handle(std::shared_ptr<Scope> scope)
        {
            auto result = source(scope);
            auto currentScope = Scope::NewScope(scope, result);

            for (auto& c : chain) {
                result = c(currentScope);
                currentScope = Scope::NewScope(currentScope, result);
            }

            return result;
        }

    }
}
