//
// Created by desrumaux on 30/11/2021.
//

#ifndef POCOJSONPATH_ROOTPEGHANDLER_HPP
#define POCOJSONPATH_ROOTPEGHANDLER_HPP

#include "AbstractPegHandler.hpp"

namespace PocoJsonPath {
    namespace Peg {

        class RootPegHandler : public AbstractPegHandler {
        public:

            RootPegHandler(std::shared_ptr<Engine> engine);

            virtual Poco::Dynamic::Var handle(std::shared_ptr<Scope> scope);

        };

    }
}

#endif //POCOJSONPATH_ROOTPEGHANDLER_HPP
