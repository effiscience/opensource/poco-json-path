//
// Created by matthias on 07/12/2021.
//

#include "ConstantePrePegHandler.hpp"

#include "ConstantePegHandler.hpp"

namespace PocoJsonPath {
    namespace Peg {

        ConstantePrePegHandler::ConstantePrePegHandler(std::shared_ptr<Engine> engine, Poco::Dynamic::Var value)
            : AbstractPrePegHandler(engine)
            , value(value)
        {}

        std::shared_ptr<AbstractPegHandler> ConstantePrePegHandler::handle(const peg::SemanticValues& sv)
        {
            return std::make_shared<ConstantePegHandler>(engine, value);
        }

    }
}