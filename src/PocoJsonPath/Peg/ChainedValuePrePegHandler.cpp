//
// Created by matthias on 07/12/2021.
//

#include "ChainedValuePrePegHandler.hpp"
#include "ChainedValuePegHandler.hpp"

namespace PocoJsonPath {
    namespace Peg {

        ChainedValuePrePegHandler::ChainedValuePrePegHandler(std::shared_ptr<Engine> engine)
            : AbstractPrePegHandler(engine)
        {}

        std::shared_ptr<AbstractPegHandler> ChainedValuePrePegHandler::handle(const peg::SemanticValues& sv)
        {
            auto source = std::any_cast<Engine::PathFunction>(sv[0]);
            std::vector<Engine::PathFunction> chain;

            for (int i = 1; i < sv.size(); i++) {
                chain.push_back(std::any_cast<Engine::PathFunction>(sv[i]));
            }

            return std::make_shared<ChainedValuePegHandler>(engine, source, chain);
        }

    }
}
