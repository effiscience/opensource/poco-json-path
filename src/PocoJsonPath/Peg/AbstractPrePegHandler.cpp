//
// Created by desrumaux on 30/11/2021.
//

#include "AbstractPrePegHandler.hpp"

namespace PocoJsonPath {
    namespace Peg {

        AbstractPrePegHandler::AbstractPrePegHandler(std::shared_ptr<Engine> engine)
            : engine(engine)
        {}

        std::shared_ptr<Engine> AbstractPrePegHandler::getEngine()
        {
            return engine;
        }

    }
}
