//
// Created by matthias on 20/12/2021.
//

#include "ObjectPrePegHandler.hpp"

#include "ObjectPegHandler.hpp"

namespace PocoJsonPath {
    namespace Peg {

        ObjectPrePegHandler::ObjectPrePegHandler(std::shared_ptr<Engine> engine)
            : AbstractPrePegHandler(engine)
        {}

        std::shared_ptr<AbstractPegHandler> ObjectPrePegHandler::handle(const peg::SemanticValues &sv)
        {
            std::vector<std::pair<Engine::PathFunction, Engine::PathFunction>> values;
            for (size_t i = 0; i < sv.size(); i+=2) {
                values.emplace_back(std::make_pair(
                            std::any_cast<Engine::PathFunction>(sv[i]),
                            std::any_cast<Engine::PathFunction>(sv[i+1])
                        ));
            }
            return std::make_shared<ObjectPegHandler>(engine, values);
        }

    }
}