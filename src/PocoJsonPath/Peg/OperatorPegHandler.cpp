//
// Created by matthias on 07/12/2021.
//

#include "OperatorPegHandler.hpp"

namespace PocoJsonPath {
    namespace Peg {

        OperatorPegHandler::OperatorPegHandler(std::shared_ptr<Engine> engine, Engine::PathFunction leftMember, std::vector<std::tuple<Engine::PathFunction, Engine::PathFunction>> operations)
            : AbstractPegHandler(engine)
            , leftMember(leftMember)
            , operations(operations)
        {}

        Poco::Dynamic::Var OperatorPegHandler::handle(std::shared_ptr<Scope> scope)
        {
            auto result = leftMember(scope);

            for (auto op : operations) {
                auto operation = std::get<0>(op)(scope).extract<std::string>();
                auto operationHandler = engine->getOperatorHandler(operation);
                if (nullptr == operationHandler) {
                    throw std::logic_error("Nullptr handler for " + operation + " operation.");
                }

                auto right = std::get<1>(op)(scope);
                result = operationHandler->invoke(*scope, result, right);
            }

            return result;
        }

    }
}
