//
// Created by desrumaux on 30/11/2021.
//

#include "StringPrePegHandler.hpp"

#include "ConstantePegHandler.hpp"
#include "PocoJsonPath/Helpers/StringsHelper.hpp"

#include <iostream>

namespace PocoJsonPath {
    namespace Peg {

        StringPrePegHandler::StringPrePegHandler(std::shared_ptr<Engine> engine)
            : AbstractPrePegHandler(engine)
        {}

        std::shared_ptr<AbstractPegHandler> StringPrePegHandler::handle(const peg::SemanticValues& sv)
        {
            auto string = sv.token_to_string();
            if (string.at(0) == '"') {
                string = Helpers::StringsHelper::undoubleQuote(string);
            }
            return std::make_shared<ConstantePegHandler>(engine, string);
        }

    }
}
