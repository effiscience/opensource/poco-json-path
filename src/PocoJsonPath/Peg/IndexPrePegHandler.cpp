//
// Created by desrumaux on 30/11/2021.
//

#include "IndexPrePegHandler.hpp"

#include "IndexPegHandler.hpp"

namespace PocoJsonPath {
    namespace Peg {

        IndexPrePegHandler::IndexPrePegHandler(std::shared_ptr<Engine> engine)
            : AbstractPrePegHandler(engine)
        {}

        std::shared_ptr<AbstractPegHandler> IndexPrePegHandler::handle(const peg::SemanticValues& sv)
        {
            return std::make_shared<IndexPegHandler>(engine, std::any_cast<Engine::PathFunction>(sv[0]));
        }

    }
}
