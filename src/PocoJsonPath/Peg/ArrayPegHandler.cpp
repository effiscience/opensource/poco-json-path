//
// Created by matthias on 20/12/2021.
//

#include "ArrayPegHandler.hpp"

#include <Poco/JSON/Array.h>

namespace PocoJsonPath {
    namespace Peg {

        ArrayPegHandler::ArrayPegHandler(std::shared_ptr<Engine> engine, std::vector<Engine::PathFunction> values)
            : AbstractPegHandler(engine)
            , values(values)
        {}

        Poco::Dynamic::Var ArrayPegHandler::handle(std::shared_ptr<Scope> scope)
        {
            Poco::JSON::Array::Ptr array{new Poco::JSON::Array};
            for (auto& value : values) {
                array->add(value(scope));
            }
            return array;
        }

    }
}
