//
// Created by desrumaux on 30/11/2021.
//

#include "IndexPegHandler.hpp"

#include <sstream>

#include "PocoJsonPath/Helpers/JsonHelper.hpp"
#include "PocoJsonPath/Variables.hpp"

namespace PocoJsonPath {
    namespace Peg {

        IndexPegHandler::IndexPegHandler(std::shared_ptr<Engine> engine, Engine::PathFunction keyword)
            : AbstractPegHandler(engine)
            , keyword(keyword)
        {}

        Poco::Dynamic::Var IndexPegHandler::handle(std::shared_ptr<Scope> scope)
        {
            auto item = scope->getValue();
            auto mbDict = Helpers::JsonHelper::castToJsonObject(item);
            auto mbArr = Helpers::JsonHelper::castToJsonArray(item);

            std::vector<std::string> variablesKeys;
            #define _(key, value) \
                variablesKeys.push_back(key); \
                scope->setVariable(key, value)

            if (mbDict.has_value()) {
                auto dict = *mbDict;
                _(Variables::length, dict->size());
            } else if (mbArr.has_value()) {
                auto dict = *mbArr;
                _(Variables::length, dict->size());
            }

            #undef _

            auto keyword = this->keyword(scope);

            for (auto& key : variablesKeys) {
                scope->deleteVariable(key);
            }

            if (mbDict.has_value()) {
                auto dict = *mbDict;
                std::string key;

                if (keyword.isString()) {
                    key = keyword.extract<std::string>();
                }

                if (keyword.isNumeric()) {
                    auto num = Helpers::JsonHelper::castToDouble(keyword);
                    if (!num.has_value()) {
                        return {};
                    }

                    std::ostringstream oss;
                    oss << num.value();
                    key = oss.str();
                }

                if (dict->has(key)) {
                    return dict->get(key);
                } else {
                    return {};
                }
            }

            if (mbArr.has_value()) {
                auto arr = *mbArr;
                int index = -1;

                if (keyword.isString()) {
                    try {
                        index = std::stod(keyword.extract<std::string>());
                    } catch (const std::exception&) {
                        return {};
                    }
                }

                if (keyword.isNumeric()) {
                    index = Helpers::JsonHelper::castToDouble(keyword).value_or(-1);
                }

                if (index >= 0 && index < arr->size()) {
                    return arr->get(index);
                } else {
                    return {};
                }
            }

            return {};
        }

    }
}
