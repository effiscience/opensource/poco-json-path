//
// Created by desrumaux on 30/11/2021.
//

#ifndef POCOJSONPATH_CURRENTPEGHANDLER_HPP
#define POCOJSONPATH_CURRENTPEGHANDLER_HPP

#include "AbstractPegHandler.hpp"

namespace PocoJsonPath {
    namespace Peg {

        class CurrentPegHandler : public AbstractPegHandler {
        public:

            CurrentPegHandler(std::shared_ptr<Engine> engine);

            virtual Poco::Dynamic::Var handle(std::shared_ptr<Scope> scope);

        };

    }
}


#endif //POCOJSONPATH_CURRENTPEGHANDLER_HPP
