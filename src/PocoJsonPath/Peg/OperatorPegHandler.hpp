//
// Created by matthias on 07/12/2021.
//

#ifndef POCOJSONPATH_OPERATORPEGHANDLER_HPP
#define POCOJSONPATH_OPERATORPEGHANDLER_HPP

#include "AbstractPegHandler.hpp"

#include "PocoJsonPath/Operators/IOperator.hpp"

namespace PocoJsonPath {
    namespace Peg {

        class OperatorPegHandler : public AbstractPegHandler {
        protected:

            Engine::PathFunction leftMember;

            std::vector<std::tuple<Engine::PathFunction, Engine::PathFunction>> operations;

        public:

            OperatorPegHandler(std::shared_ptr<Engine> engine, Engine::PathFunction leftMember, std::vector<std::tuple<Engine::PathFunction, Engine::PathFunction>> operations);

            virtual Poco::Dynamic::Var handle(std::shared_ptr<Scope> scope);

        };

    }
}


#endif //POCOJSONPATH_OPERATORPEGHANDLER_HPP
