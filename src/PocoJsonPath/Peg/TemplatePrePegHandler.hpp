//
// Created by desrumaux on 30/11/2021.
//

#ifndef POCOJSONPATH_TEMPLATEPREPEGHANDLER_HPP
#define POCOJSONPATH_TEMPLATEPREPEGHANDLER_HPP

#include "AbstractPrePegHandler.hpp"

namespace PocoJsonPath {
    namespace Peg {

        template <class Type, bool SvPropagation = false>
        class TemplatePrePegHandler : public AbstractPrePegHandler {
        public:

            TemplatePrePegHandler(std::shared_ptr<Engine> engine)
                : AbstractPrePegHandler(engine)
            {}

            std::shared_ptr<AbstractPegHandler> handle(const peg::SemanticValues& sv)
            {
                if constexpr (SvPropagation) {
                    return std::make_shared<Type>(engine, sv);
                } else {
                    return std::make_shared<Type>(engine);
                }
            }

        };

    }
}

#endif //POCOJSONPATH_TEMPLATEPREPEGHANDLER_HPP
