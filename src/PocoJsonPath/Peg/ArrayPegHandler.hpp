//
// Created by matthias on 20/12/2021.
//

#ifndef POCOJSONPATH_ARRAYPEGHANDLER_HPP
#define POCOJSONPATH_ARRAYPEGHANDLER_HPP

#include "AbstractPegHandler.hpp"

#include <vector>

namespace PocoJsonPath {
    namespace Peg {

        class ArrayPegHandler : public AbstractPegHandler {
            std::vector<Engine::PathFunction> values;

        public:

            ArrayPegHandler(std::shared_ptr<Engine> engine, std::vector<Engine::PathFunction> values);

            virtual Poco::Dynamic::Var handle(std::shared_ptr<Scope> scope);
        };

    }
}

#endif //POCOJSONPATH_ARRAYPEGHANDLER_HPP
