//
// Created by matthias on 07/12/2021.
//

#ifndef POCOJSONPATH_CHAINEDVALUEPREPEGHANDLER_HPP
#define POCOJSONPATH_CHAINEDVALUEPREPEGHANDLER_HPP

#include "AbstractPrePegHandler.hpp"

namespace PocoJsonPath {
    namespace Peg {

        class ChainedValuePrePegHandler : public AbstractPrePegHandler {
        public:

            ChainedValuePrePegHandler(std::shared_ptr<Engine> engine);

            virtual std::shared_ptr<AbstractPegHandler> handle(const peg::SemanticValues& sv);

        };

    }
}


#endif //POCOJSONPATH_CHAINEDVALUEPREPEGHANDLER_HPP
