//
// Created by matthias on 07/12/2021.
//

#ifndef POCOJSONPATH_CONSTANTEPREPEGHANDLER_HPP
#define POCOJSONPATH_CONSTANTEPREPEGHANDLER_HPP

#include "AbstractPrePegHandler.hpp"

namespace PocoJsonPath {
    namespace Peg {

        class ConstantePrePegHandler : public AbstractPrePegHandler {
            Poco::Dynamic::Var value;

        public:

            ConstantePrePegHandler(std::shared_ptr<Engine> engine, Poco::Dynamic::Var value);

            virtual std::shared_ptr<AbstractPegHandler> handle(const peg::SemanticValues& sv);

        };

    }
}


#endif //POCOJSONPATH_CONSTANTEPREPEGHANDLER_HPP
