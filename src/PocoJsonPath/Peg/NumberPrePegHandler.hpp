//
// Created by desrumaux on 30/11/2021.
//

#ifndef POCOJSONPATH_NUMBERPREPEGHANDLER_HPP
#define POCOJSONPATH_NUMBERPREPEGHANDLER_HPP

#include "AbstractPrePegHandler.hpp"

namespace PocoJsonPath {
    namespace Peg {

        class NumberPrePegHandler : public AbstractPrePegHandler {
        public:

            NumberPrePegHandler(std::shared_ptr<Engine> engine);

            virtual std::shared_ptr<AbstractPegHandler> handle(const peg::SemanticValues& sv);

        };

    }
}


#endif //POCOJSONPATH_NUMBERPREPEGHANDLER_HPP
