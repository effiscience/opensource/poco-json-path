//
// Created by matthias on 20/12/2021.
//

#ifndef POCOJSONPATH_OBJECTPEGHANDLER_HPP
#define POCOJSONPATH_OBJECTPEGHANDLER_HPP

#include "AbstractPegHandler.hpp"

#include <vector>

namespace PocoJsonPath {
    namespace Peg {

        class ObjectPegHandler : public AbstractPegHandler {
            std::vector<std::pair<Engine::PathFunction, Engine::PathFunction>> values;

        public:

            ObjectPegHandler(std::shared_ptr<Engine> engine, std::vector<std::pair<Engine::PathFunction, Engine::PathFunction>> values);

            virtual Poco::Dynamic::Var handle(std::shared_ptr<Scope> scope);
        };

    }
}


#endif //POCOJSONPATH_OBJECTPEGHANDLER_HPP
