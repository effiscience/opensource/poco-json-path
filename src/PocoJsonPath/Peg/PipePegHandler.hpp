//
// Created by matthias on 07/12/2021.
//

#ifndef POCOJSONPATH_PIPEPEGHANDLER_HPP
#define POCOJSONPATH_PIPEPEGHANDLER_HPP

#include "AbstractPegHandler.hpp"

namespace PocoJsonPath {
    namespace Peg {

        class PipePegHandler : public AbstractPegHandler {
        protected:

            Engine::PathFunction pipeName;

            std::vector<Engine::PathFunction> variables;

        public:

            PipePegHandler(std::shared_ptr<Engine> engine, Engine::PathFunction pipeName, std::vector<Engine::PathFunction> variables);

            virtual Poco::Dynamic::Var handle(std::shared_ptr<Scope> scope);

        };

    }
}

#endif //POCOJSONPATH_PIPEPEGHANDLER_HPP
