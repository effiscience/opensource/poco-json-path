//
// Created by matthias on 20/12/2021.
//

#ifndef POCOJSONPATH_OBJECTPREPEGHANDLER_HPP
#define POCOJSONPATH_OBJECTPREPEGHANDLER_HPP

#include "AbstractPrePegHandler.hpp"

namespace PocoJsonPath {
    namespace Peg {

        class ObjectPrePegHandler : public AbstractPrePegHandler {
        public:

            ObjectPrePegHandler(std::shared_ptr<Engine> engine);

            virtual std::shared_ptr<AbstractPegHandler> handle(const peg::SemanticValues &sv);

        };

    }
}


#endif //POCOJSONPATH_OBJECTPREPEGHANDLER_HPP
