//
// Created by desrumaux on 30/11/2021.
//

#include "CurrentPegHandler.hpp"

namespace PocoJsonPath {
    namespace Peg {

        CurrentPegHandler::CurrentPegHandler(std::shared_ptr<Engine> engine)
            : AbstractPegHandler(engine)
        {}

        Poco::Dynamic::Var CurrentPegHandler::handle(std::shared_ptr<Scope> scope)
        {
            return scope->getValue();
        }

    }
}
