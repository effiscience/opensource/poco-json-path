//
// Created by matthias on 07/12/2021.
//

#include "VariablePegHandler.hpp"

namespace PocoJsonPath {
    namespace Peg {

        VariablePegHandler::VariablePegHandler(std::shared_ptr<Engine> engine, Engine::PathFunction key, bool root)
            : AbstractPegHandler(engine)
            , root(root)
            , key(key)
        {}

        Poco::Dynamic::Var VariablePegHandler::handle(std::shared_ptr<Scope> scope)
        {
            auto k = key(scope);

            if (root) {
                if (!scope->isRootScope()) {
                    scope = scope->getRoot();
                }
            }

            return scope->getVariable(k.extract<std::string>());
        }

    }
}
