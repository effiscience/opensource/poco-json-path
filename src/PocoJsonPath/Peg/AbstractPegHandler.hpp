//
// Created by desrumaux on 23/11/2021.
//

#ifndef POCOJSONPATH_ABSTRACTPEGHANDLER_HPP
#define POCOJSONPATH_ABSTRACTPEGHANDLER_HPP

#include <peglib.h>

#include "PocoJsonPath/Engine.hpp"
#include "PocoJsonPath/Scope.hpp"

namespace PocoJsonPath {
    namespace Peg {

        class AbstractPegHandler {
        protected:

            std::shared_ptr<Engine> engine;

        public:

            AbstractPegHandler(std::shared_ptr<Engine> engine);

            std::shared_ptr<Engine> getEngine();

            virtual Poco::Dynamic::Var handle(std::shared_ptr<Scope> scope) = 0;

        };

    }
}


#endif //POCOJSONPATH_ABSTRACTPEGHANDLER_HPP
