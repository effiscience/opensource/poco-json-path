//
// Created by matthias on 13/12/2021.
//

#ifndef POCOJSONPATH_RECURSIVECHAINEDVALUEPREPEGHANDLER_HPP
#define POCOJSONPATH_RECURSIVECHAINEDVALUEPREPEGHANDLER_HPP

#include "AbstractPrePegHandler.hpp"

namespace PocoJsonPath {
    namespace Peg {

        class RecursiveChainedValuePrePegHandler : public AbstractPrePegHandler {
            bool isFilter;

        public:

            RecursiveChainedValuePrePegHandler(std::shared_ptr<Engine> engine, bool isFilter);

            virtual std::shared_ptr<AbstractPegHandler> handle(const peg::SemanticValues& sv);

        };

    }
}


#endif //POCOJSONPATH_RECURSIVECHAINEDVALUEPREPEGHANDLER_HPP
