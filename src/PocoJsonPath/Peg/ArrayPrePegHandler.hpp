//
// Created by matthias on 20/12/2021.
//

#ifndef POCOJSONPATH_ARRAYPREPEGHANDLER_HPP
#define POCOJSONPATH_ARRAYPREPEGHANDLER_HPP

#include "AbstractPrePegHandler.hpp"

namespace PocoJsonPath {
    namespace Peg {

        class ArrayPrePegHandler : public AbstractPrePegHandler {
        public:

            ArrayPrePegHandler(std::shared_ptr<Engine> engine);

            virtual std::shared_ptr<AbstractPegHandler> handle(const peg::SemanticValues &sv);

        };

    }
}


#endif //POCOJSONPATH_ARRAYPREPEGHANDLER_HPP
