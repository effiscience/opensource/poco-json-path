//
// Created by matthias on 07/12/2021.
//

#ifndef POCOJSONPATH_PIPEPREPEGHANDLER_HPP
#define POCOJSONPATH_PIPEPREPEGHANDLER_HPP

#include "AbstractPrePegHandler.hpp"

namespace PocoJsonPath {
    namespace Peg {

        class PipePrePegHandler : public AbstractPrePegHandler {
        public:

            PipePrePegHandler(std::shared_ptr<Engine> engine);

            virtual std::shared_ptr<AbstractPegHandler> handle(const peg::SemanticValues& sv);

        };

    }
}

#endif //POCOJSONPATH_PIPEPREPEGHANDLER_HPP
