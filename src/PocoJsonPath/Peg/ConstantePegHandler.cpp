//
// Created by desrumaux on 30/11/2021.
//

#include "ConstantePegHandler.hpp"

namespace PocoJsonPath {
    namespace Peg {

        ConstantePegHandler::ConstantePegHandler(std::shared_ptr<Engine> engine, Poco::Dynamic::Var value)
            : AbstractPegHandler(engine)
            , value(value)
        {}

        Poco::Dynamic::Var ConstantePegHandler::handle(std::shared_ptr<Scope> scope)
        {
            return value;
        }

    }
}
