//
// Created by matthias on 07/12/2021.
//

#include "OperatorPrePegHandler.hpp"

#include "OperatorPegHandler.hpp"

namespace PocoJsonPath {
    namespace Peg {

        OperatorPrePegHandler::OperatorPrePegHandler(std::shared_ptr<Engine> engine)
            : AbstractPrePegHandler(engine)
        {}

        std::shared_ptr<AbstractPegHandler> OperatorPrePegHandler::handle(const peg::SemanticValues& sv)
        {
            auto leftMember = std::any_cast<Engine::PathFunction>(sv[0]);
            std::vector<std::tuple<Engine::PathFunction, Engine::PathFunction>> operations;

            for (int i = 1; i < sv.size(); i += 2) {
                operations.push_back(std::make_tuple(
                            std::any_cast<Engine::PathFunction>(sv[i]),
                            std::any_cast<Engine::PathFunction>(sv[i+1])
                        ));
            }

            return std::make_shared<OperatorPegHandler>(engine, leftMember, operations);
        }

    }
}
