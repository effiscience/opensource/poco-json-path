//
// Created by matthias on 07/12/2021.
//

#ifndef POCOJSONPATH_FILTERPREPEGHANDLER_HPP
#define POCOJSONPATH_FILTERPREPEGHANDLER_HPP

#include "AbstractPrePegHandler.hpp"

namespace PocoJsonPath {
    namespace Peg {

        class FilterPrePegHandler : public AbstractPrePegHandler {
        public:

            FilterPrePegHandler(std::shared_ptr<Engine> engine);

            virtual std::shared_ptr<AbstractPegHandler> handle(const peg::SemanticValues& sv);

        };

    }
}


#endif //POCOJSONPATH_FILTERPREPEGHANDLER_HPP
