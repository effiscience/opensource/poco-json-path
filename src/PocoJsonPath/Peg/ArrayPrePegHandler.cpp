//
// Created by matthias on 20/12/2021.
//

#include "ArrayPrePegHandler.hpp"

#include "ArrayPegHandler.hpp"

namespace PocoJsonPath {
    namespace Peg {

        ArrayPrePegHandler::ArrayPrePegHandler(std::shared_ptr<Engine> engine)
            : AbstractPrePegHandler(engine)
        {}

        std::shared_ptr<AbstractPegHandler> ArrayPrePegHandler::handle(const peg::SemanticValues &sv)
        {
            std::vector<Engine::PathFunction> values;
            for (size_t i = 0; i < sv.size(); i++) {
                values.push_back(std::any_cast<Engine::PathFunction>(sv[i]));
            }
            return std::make_shared<ArrayPegHandler>(engine, values);
        }

    }
}
