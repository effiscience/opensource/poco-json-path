//
// Created by matthias on 07/12/2021.
//

#ifndef POCOJSONPATH_OPERATORPREPEGHANDLER_HPP
#define POCOJSONPATH_OPERATORPREPEGHANDLER_HPP

#include "AbstractPrePegHandler.hpp"

namespace PocoJsonPath {
    namespace Peg {

        class OperatorPrePegHandler : public AbstractPrePegHandler {
        public:

            OperatorPrePegHandler(std::shared_ptr<Engine> engine);

            virtual std::shared_ptr<AbstractPegHandler> handle(const peg::SemanticValues& sv);

        };

    }
}


#endif //POCOJSONPATH_OPERATORPREPEGHANDLER_HPP
