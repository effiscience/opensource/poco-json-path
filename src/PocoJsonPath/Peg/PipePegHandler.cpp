//
// Created by matthias on 07/12/2021.
//

#include "PipePegHandler.hpp"

#include "PocoJsonPath/Filters/IFilter.hpp"

namespace PocoJsonPath {
    namespace Peg {

        PipePegHandler::PipePegHandler(std::shared_ptr<Engine> engine, Engine::PathFunction pipeName, std::vector<Engine::PathFunction> variables)
            : AbstractPegHandler(engine)
            , pipeName(pipeName)
            , variables(variables)
        {}

        Poco::Dynamic::Var PipePegHandler::handle(std::shared_ptr<Scope> scope)
        {
            auto name = pipeName(scope).extract<std::string>();
            auto pipeHandler = engine->getFilterHandler(name);
            if (nullptr == pipeHandler) {
                throw std::logic_error("No filter for " + name + " keyword.");
            }

            std::vector<Poco::Dynamic::Var> variables;
            for (auto& variable : this->variables) {
                variables.push_back(variable(scope));
            }

            return pipeHandler->invoke(engine, scope->getValue(), variables);
        }

    }
}