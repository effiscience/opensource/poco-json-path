//
// Created by matthias on 07/12/2021.
//

#ifndef POCOJSONPATH_CHAINEDVALUEPEGHANDLER_HPP
#define POCOJSONPATH_CHAINEDVALUEPEGHANDLER_HPP

#include "AbstractPegHandler.hpp"

namespace PocoJsonPath {
    namespace Peg {

        class ChainedValuePegHandler : public AbstractPegHandler {
        protected:

            Engine::PathFunction source;

            std::vector<Engine::PathFunction> chain;

        public:

            ChainedValuePegHandler(std::shared_ptr<Engine> engine, Engine::PathFunction source, std::vector<Engine::PathFunction> chain);

            virtual Poco::Dynamic::Var handle(std::shared_ptr<Scope> scope);

        };

    }
}



#endif //POCOJSONPATH_CHAINEDVALUEPEGHANDLER_HPP
