//
// Created by matthias on 07/12/2021.
//

#include "VariablePrePegHandler.hpp"

#include "VariablePegHandler.hpp"

namespace PocoJsonPath {
    namespace Peg {

        VariablePrePegHandler::VariablePrePegHandler(std::shared_ptr<Engine> engine, bool root)
            : AbstractPrePegHandler(engine)
            , root(root)
        {}

        std::shared_ptr<AbstractPegHandler> VariablePrePegHandler::handle(const peg::SemanticValues& sv)
        {
            return std::make_shared<VariablePegHandler>(engine, std::any_cast<Engine::PathFunction>(sv[0]), root);
        }

    }
}
