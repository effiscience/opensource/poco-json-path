//
// Created by matthias on 21/12/2021.
//

#include "StringsHelper.hpp"

#include <sstream>

#include <Poco/RegularExpression.h>

namespace PocoJsonPath {
    namespace Helpers {

        std::string StringsHelper::escape(const std::string& original, const std::map<char, char>& characters, const char& escapeCharacter)
        {
            std::ostringstream oss;
            for (const auto& c : original) {
                auto match = characters.find(c);
                if (characters.end() != match) {
                    oss << escapeCharacter << match->second;
                } else if (c == escapeCharacter) {
                    oss << escapeCharacter << escapeCharacter;
                } else {
                    oss << c;
                }
            }
            return oss.str();
        }

        std::string StringsHelper::unescape(const std::string& original, const std::map<char, char>& characters, const char& escapeCharacter)
        {
            std::ostringstream oss;
            bool escape = false;
            for (const auto& c : original) {
                if (escape) {
                    auto match = characters.find(c);
                    if (characters.end() != match) {
                        oss << match->second;
                    } else if (c == escapeCharacter) {
                        oss << escapeCharacter;
                    } else {
                        oss << c;
                    }
                    escape = false;
                } else if (c == escapeCharacter) {
                    escape = true;
                } else {
                    oss << c;
                }
            }
            return oss.str();
        }

        std::string StringsHelper::quote(const std::string& original)
        {
            return '\'' + escape(original, {
                {'n', '\n'},
                {'t', '\t'},
                {'r', '\r'},
                {'\'', '\''},
            }) + '\'';
        }

        std::string StringsHelper::unquote(const std::string& original)
        {
            if (original.front() != '\'' || original.back() != '\'' || original.size() < 2) {
                throw std::invalid_argument("Unable to unquote a malformed string.");
            }

            return unescape(original.substr(1, original.size() - 2), {
                    {'n', '\n'},
                    {'t', '\t'},
                    {'r', '\r'},
                    {'\'', '\''},
            });
        }

        std::string StringsHelper::doubleQuote(const std::string& original)
        {
            return '\"' + escape(original, {
                    {'\n', 'n'},
                    {'\t', 't'},
                    {'\r', 'r'},
                    {'\"', '\"'},
            }) + '\"';
        }

        std::string StringsHelper::undoubleQuote(const std::string& original)
        {
            if (original.front() != '\"' || original.back() != '\"' || original.size() < 2) {
                throw std::invalid_argument("Unable to unquote a malformed string.");
            }

            return unescape(original.substr(1, original.size() - 2), {
                    {'n', '\n'},
                    {'t', '\t'},
                    {'r', '\r'},
                    {'\"', '\"'},
            });
        }

        bool StringsHelper::contains(const std::string& subject, const std::string& target)
        {
            if (subject.size() > target.size()) {
                return false;
            }

            auto escapeSubject = escape(subject, {
                {'[', '['},
                {']', ']'},
                {'(', ')'},
                {'.', '.'},
                {'!', '!'},
                {'*', '*'},
                {'+', '+'},
                {'>', '>'},
                {'<', '<'},
            });
            Poco::RegularExpression::Match matches;
            Poco::RegularExpression re{escapeSubject};
            return re.match(target, 0, matches);
        }
    }
}