//
// Created by matthias on 21/12/2021.
//

#ifndef POCOJSONPATH_STRINGSHELPER_HPP
#define POCOJSONPATH_STRINGSHELPER_HPP

#include <map>
#include <string>

namespace PocoJsonPath {
    namespace Helpers {

        namespace StringsHelper {
            std::string escape(const std::string& original, const std::map<char, char>& characters, const char& escapeCharacter = '\\');
            std::string unescape(const std::string& original, const std::map<char, char>& characters, const char& escapeCharacter = '\\');
            std::string quote(const std::string& original);
            std::string unquote(const std::string& original);
            std::string doubleQuote(const std::string& original);
            std::string undoubleQuote(const std::string& original);
            bool contains(const std::string& subject, const std::string& target);
        }

    }
}

#endif //POCOJSONPATH_STRINGSHELPER_HPP
